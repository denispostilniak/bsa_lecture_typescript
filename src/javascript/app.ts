import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import {Fighter} from './models/fighter'
import { FighterDetails } from './models/fighterDetails';

const rootElement: HTMLElement = <HTMLElement>document.getElementById('root');
const loadingElement: HTMLElement = <HTMLElement>document.getElementById('loading-overlay');

export async function startApp() {
  try {
    loadingElement.style.visibility = 'visible';
    
    const fighters: FighterDetails[]  = await getFighters();
    const fightersElement = createFighters(fighters);

    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    loadingElement.style.visibility = 'hidden';
  }
}
