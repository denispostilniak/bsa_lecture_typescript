import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../models/fighter'
import { FighterDetails } from '../models/fighterDetails';

export async function getFighters(): Promise<FighterDetails[]>  {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return <FighterDetails[]>apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<FighterDetails> {
  try{
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');

    return <FighterDetails>apiResult;
  } catch(error){
   throw error;
  }
}

