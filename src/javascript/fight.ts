import { Fighter } from "./models/fighter";
import { FighterDetails } from "./models/fighterDetails";

export function fight(firstFighter: FighterDetails, secondFighter: FighterDetails) {
  var isFirstFighter: Boolean = true;
  var winner: FighterDetails;
  var firstFighterHealth = <number>firstFighter.health;
  var secondFighterHealth = <number>secondFighter.health;
  while(true){

    if(isFirstFighter){
      let damage = getDamage(firstFighter, secondFighter);
      secondFighterHealth -= damage;
      isFirstFighter = false;
    }

    if(secondFighterHealth <= 0){
      winner = firstFighter;
      break;
    }

    if(!isFirstFighter){
      let damage = getDamage(secondFighter, firstFighter);
      firstFighterHealth -= damage;
      isFirstFighter = true;
    }

    if(firstFighterHealth <= 0){
      winner = secondFighter;
      break;
    }
  }
  return <Fighter>winner;
}

export function getDamage(attacker: FighterDetails, enemy: FighterDetails) {
  const hit = getHitPower(attacker);
  const block = getBlockPower(enemy);

  const damage = hit - block;
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter: FighterDetails) {
  const criticalHitChance = (Math.random() * 2 + 1);

  return <number>fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: FighterDetails) {
  const dodgeChance = (Math.random() * 2 + 1);
  
  return <number>fighter.defense * dodgeChance;
}
