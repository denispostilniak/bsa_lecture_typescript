import { Fighter } from '../models/fighter'
import { FighterDetails } from '../models/fighterDetails';
import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter: Fighter) {
  const title: string = 'Winner info';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter: Fighter){
  const { name, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes: {} });

  const nameElement = createElement({ tagName: 'span', className: 'fighter-name', attributes: {} });
  const imageElement = createElement({tagName: 'img', className: 'fighter-image', attributes: {src: source} });
  
  // show fighter name, image

  nameElement.innerText = name;
  fighterDetails.append(nameElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}