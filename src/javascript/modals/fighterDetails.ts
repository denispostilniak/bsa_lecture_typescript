import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter } from '../models/fighter';
import { FighterDetails } from '../models/fighterDetails';

export  function showFighterDetailsModal(fighter: FighterDetails) {
  const title: string = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FighterDetails) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes: {} });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name', attributes: {} });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: source} });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-details', attributes: {} });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-details', attributes: {} });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-details', attributes: {} });

  
  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  attackElement.innerText = `Attack is ${attack}`;
  defenseElement.innerText = `Defense is ${defense}`;
  healthElement.innerText = `Health is ${health}`;
  fighterDetails.append(nameElement);
  fighterDetails.append(imageElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);

  return fighterDetails;
}
