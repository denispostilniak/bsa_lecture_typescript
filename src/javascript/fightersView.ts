import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { Fighter } from './models/fighter';
import { getFighterDetails } from '../javascript/services/fightersService'
import { fighters, fightersDetails } from './helpers/mockData';
import { FighterDetails } from './models/fighterDetails';

export function createFighters(fighters: FighterDetails[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(<Fighter>fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters', attributes: {} });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event: Event, fighter: Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string) {
  return getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map<string, FighterDetails>();

  return async function selectFighterForBattle(event: any, fighter: Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const fighters = selectedFighters.values();
      const winner = fight(fighters.next().value, fighters.next().value);
      showWinnerModal(winner);
    }
  }
}
