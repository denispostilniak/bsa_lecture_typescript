import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showWinnerModal(fighter) {
    const title = 'Winner info';
    const bodyElement = createWinnerDetails(fighter);
    showModal({ title, bodyElement });
}
function createWinnerDetails(fighter) {
    const { name, source } = fighter;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes: {} });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name-detail', attributes: {} });
    const imageElement = createElement({ tagName: 'image', className: 'fighter-image', attributes: { source } });
    // show fighter name, image
    nameElement.innerText = name;
    fighterDetails.append(nameElement);
    fighterDetails.append(imageElement);
    return fighterDetails;
}
