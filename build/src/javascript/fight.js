export function fight(firstFighter, secondFighter) {
    var isFirstFighter = true;
    var winner;
    var firstFighterHealth = firstFighter.health;
    var secondFighterHealth = secondFighter.health;
    while (true) {
        if (isFirstFighter) {
            let damage = getDamage(firstFighter, secondFighter);
            secondFighterHealth -= damage;
            isFirstFighter = false;
        }
        if (!isFirstFighter) {
            let damage = getDamage(secondFighter, firstFighter);
            firstFighterHealth -= damage;
            isFirstFighter = true;
        }
        if (firstFighterHealth <= 0) {
            winner = secondFighter;
            break;
        }
        if (secondFighterHealth <= 0) {
            winner = firstFighter;
            break;
        }
    }
    return winner;
}
export function getDamage(attacker, enemy) {
    const hit = getHitPower(attacker);
    const block = getBlockPower(enemy);
    const damage = hit - block;
    return damage < 0 ? 0 : damage;
}
export function getHitPower(fighter) {
    const criticalHitChance = (Math.random() * 2 + 1);
    return fighter.attack * criticalHitChance;
}
export function getBlockPower(fighter) {
    const dodgeChance = (Math.random() * 2 + 1);
    return fighter.defense * dodgeChance;
}
